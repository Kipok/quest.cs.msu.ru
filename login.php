<?php
    include("config.php");
    session_start();

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $sql_prepared = $db->prepare("SELECT username FROM usernames WHERE username = ?");
        $sql_prepared->bind_param('s', $_POST['username']);
        $sql_prepared->execute();
        $sql_prepared->store_result();
        $sql_prepared->bind_result($myusername);

        if ($sql_prepared->fetch()) {
            $_SESSION['logged_name'] = strtolower($myusername);
            $response = "Welcome back, {$myusername}";
        } else {
            $response = "No one of such name is known here!";
        }
        $sql_prepared->close();
        echo $response;
    }
?>
