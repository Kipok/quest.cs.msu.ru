<?php
    session_start();
    if (isset($_SESSION['logged_name'])) {
        header("location:map.php");
    }
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="style/console.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="js/console.js"></script>
    <title></title>
</head>
<body onclick="activate();">
<div class="central-block">
    <h1>&gt; Enter your name:</h1>
    <div id="cmd">
        <span></span>
        <div id="cursor"></div>
    </div>
    <label>
        <input type="text" maxlength="20" class="console-input">
    </label>
</div>
</body>
</html>
