<?php
    session_start();
    if (!isset($_SESSION['logged_name'])) {
        header("location:index.php");
        exit;
    }
    // TODO: check that username exists in db
    $username = $_SESSION['logged_name'];
    include("config.php");

    $sqlp = $db->prepare("SELECT id FROM usernames WHERE username=?");
    $sqlp->bind_param('s', $username);
    $sqlp->execute();
    $sqlp->store_result();
    $sqlp->bind_result($user_id);

    if (!$sqlp->fetch()) {
        header("location:index.php");
        exit;
    }

    $layer = 0;
    if ($_GET["layer"]) {
        $layer = intval(htmlspecialchars($_GET["layer"], ENT_QUOTES, 'UTF-8'));
    }

    if($_SERVER["REQUEST_METHOD"] == "POST") {
        $myanswer = $_POST['answer'];
        $mytask = $_POST['task'];

        $myfile = fopen(strtolower("logs/{$username}-{$mytask}.txt"), "a");
        fwrite($myfile, $myanswer . "\n");
        fclose($myfile);

        $sql_prepared = $db->prepare("SELECT answer, text FROM riddles WHERE num=?");
        $sql_prepared->bind_param('i', intval($mytask));
        $sql_prepared->execute();

        $sql_prepared->store_result();
        $sql_prepared->bind_result($answer, $result);

        if ($sql_prepared->fetch() && $answer == $myanswer) {
            $sql_check_solved = $db->prepare("SELECT user_id FROM progress WHERE user_id=? AND riddle_id=?");
            $sql_check_solved->bind_param('si', $username, intval($mytask));
            $sql_check_solved->execute();

            $sql_check_solved->store_result();
            $sql_check_solved->bind_result($user_id);

            if (!$sql_check_solved->fetch()) {
                $sqlp = $db->prepare("INSERT INTO `quest.cs.msu.ru`.`progress` (`user_id`, `riddle_id`) VALUES (?, ?)");
                $sqlp->bind_param('si', $username, intval($mytask));
                $sqlp->execute();
                $sqlp->store_result();
                $sqlp->close();

                $message = "{$username} has finally solved riddle #{$mytask}!";
                mail('ivypawn@gmail.com', 'quest works :)', $message);
            }
            $sql_check_solved->close();

            $file = "files/" . $result;
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($file).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            exit;
        }
        $sql_prepared->close();
    }
?>
<html>
    <head>
        <title></title>
        <link href="style/map.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    </head>
    <body <?php echo "style=\"background-image: url(/images/floor{$layer}.png);\""?>>
        <form name="answer" method="post" action="">
            <div id="answer-div">
                <input id="answer-input" name="answer" type="text" size="40" autocomplete="off" spellcheck="false">
            </div>
            <div id="problems">
<?php
    $sqlp = $db->prepare("SELECT num, position_x, position_y, url, ip FROM riddles WHERE layer=?");
    $sqlp->bind_param('i', intval($layer));
    $sqlp->execute();

    $sqlp->store_result();
    $sqlp->bind_result($id, $x, $y, $url, $ip);
    while ($sqlp->fetch()) {
        $sql_check_solved = $db->prepare("SELECT user_id FROM progress WHERE user_id=? AND riddle_id=?");
        $sql_check_solved->bind_param('si', $username, intval($id));
        $sql_check_solved->execute();

        $sql_check_solved->store_result();
        $sql_check_solved->bind_result($user_id);
        $check = $sql_check_solved->fetch();

        $solved = ($check ? "style=\"border: 2px solid rgb(0, 40, 0);\"" : "");
        $solved1 = ($check ? "style=\"border-color: black; background: rgb(0, 100, 0);\"" : "");
        echo "<div style=\"left: {$x}%; top: {$y}%; position: absolute;\">";
        echo "<input id=\"riddle-button-{$id}\" class=\"riddle-button\" type=\"radio\" name=\"task\" value=\"{$id}\">";
        echo "<label for=\"{$id}\"><span {$solved}><span {$solved1}></span></span>";
        if ($url != "") {
            echo "<label class=\"ip-label\"><a href=\"{$url}\">{$ip}</a></label>";
        }
        echo "</label></div>";
        $sql_check_solved->close();
    }
    $sqlp->close();
    echo "</div></form>";
    echo "<script src=\"js/map.js\"></script>";
?>
        <div id="layers">
            <input type="submit" value="0" onclick="window.location='/map.php?layer=0';"/><br>
            <input type="submit" value="1" onclick="window.location='/map.php?layer=1';"/><br>
            <input type="submit" value="2" onclick="window.location='/map.php?layer=2';"/><br>
            <input type="submit" value="3" onclick="window.location='/map.php?layer=3';"/><br>
            <input type="submit" value="4" onclick="window.location='/map.php?layer=4';"/><br>
            <input type="submit" value="5" onclick="window.location='/map.php?layer=5';"/><br>
            <input type="submit" value="6" onclick="window.location='/map.php?layer=6';"/><br>
            <input type="submit" value="7" onclick="window.location='/map.php?layer=7';"/><br>
            <input type="submit" value="8" onclick="window.location='/map.php?layer=8';"/><br>
            <input type="submit" value="9" onclick="window.location='/map.php?layer=9';"/>
        </div>
    </body>
</html>
