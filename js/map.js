$('html').click(function() {
    $("#answer-div").css("display", "none");
    $("#answer-input").val("");
    $(".riddle-button").prop('checked', false);
});

$(".riddle-button").click(function(event) {
    event.stopPropagation();
    $("#answer-div").css("display", "block");
});

$("#answer-div").click(function(event) {
    event.stopPropagation();
});
