function start() {
    var cursor_elem = $('#cursor');
    var console_input = $('.console-input');

    var cursor;
    $('#cmd').click(function () {
        cursor = window.setInterval(function () {
            if (cursor_elem.css('visibility') === 'visible') {
                cursor_elem.css({
                    visibility: 'hidden'
                });
            } else {
                cursor_elem.css({
                    visibility: 'visible'
                });
            }
        }, 700);

    });

    console_input.keyup(function (e) {
        if ($(this).val().length > 30) {
            return;
        }
        $('#cmd').find('span').text($(this).val());
        if (e.keyCode == 13){
            check_input();
        }
    });

    console_input.blur(function () {
        console_input.focus();
    });

}

function activate(){
    $('.console-input').focus();
}


$(document).ready(function(){
    $('.console-input').focus();
    start();
    $('#cmd').click();
});

function replace_title(text){
    var cursor_elem = $('#cursor');
    var console_input = $('.console-input');
    var header = $('h1');
    var deleted = false;

    function stop(intId){
        clearInterval(intId);
        deleted = false;
    }

    function deleter(intId){
        if (console_input.val() != '') {
            console_input.val(console_input.val().substr(0, console_input.val().length - 1));
            console_input.keyup();
        } else if (header.text() != '') {
            header.text(header.text().substr(0, header.text().length - 1));
        } else {
            return 0;
        }
        return 1;
    }

    function inserter(intId, text){
        if (header.text() == text) {
            stop(intId);
            setTimeout( function() {
                if (text[2] == 'W') {
                    window.location.href = "map.php";
                }
            }, 3000)
        } else {
            header.text(text.substr(0, header.text().length + 1));
        }
    }

    timer = setInterval(function () {
        if (!deleted) {
            if (!deleter(timer)) {
                deleted = true;
            }
        } else {
            inserter(timer, text);
        }
    }, 30);
}

function check_input(){
    username = $('.console-input').val();
    $.ajax({
        type: 'post',
        url: 'login.php',
        data: {'username': username},
        response: 'text',
        success:function (data) {
            replace_title("> ".concat(data));
        }
    });
}
